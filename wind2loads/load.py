"""
Provides support to for using @nkdi Wind2Load toolbox.
"""


# %% Import section.

import numpy as np


# %% Dictionaries for w2l.

# Dictionary used by predict_output() to call the correct function for
# each model type.
_switch_output_update = {}

# Dictionary used by predict_gradient() to call the correct function for
# each model type.
_switch_gradient_update = {}


# %% Functions related to w2l.

try:

    import w2l
    from w2l.neuralnets import ann

    def _predict_output_w2l_ann(model, input):
        """
        Predict output function for w2l neural networks.
        """
        output = model.predict(input)
        return output

    def _predict_gradient_w2l_ann(model, input):
        """
        Predict gradient function for w2l neural networks.
        """
        output, gradient = model.backward_propagation_dy(input)
        # For now we support only Single Output surrogates.
        return gradient[0]

    # Add these functions to the models list.
    _switch_output_update[w2l.neuralnets.ann] = _predict_output_w2l_ann
    _switch_gradient_update[w2l.neuralnets.ann] = _predict_gradient_w2l_ann


except ModuleNotFoundError:
    pass


def update_switch():
    return _switch_output_update, _switch_gradient_update
